from appium import webdriver
import os, json

CONFIG_FILE = os.environ['CONFIG_FILE'] if 'CONFIG_FILE' in os.environ else 'config/single.json'
TASK_ID = int(os.environ['TASK_ID']) if 'TASK_ID' in os.environ else 0

with open(CONFIG_FILE) as data_file:
    CONFIG = json.load(data_file)

bs_local = None

BROWSERSTACK_USERNAME = os.environ['BROWSERSTACK_USERNAME'] if 'BROWSERSTACK_USERNAME' in os.environ else CONFIG['user']
BROWSERSTACK_ACCESS_KEY = os.environ['BROWSERSTACK_ACCESS_KEY'] if 'BROWSERSTACK_ACCESS_KEY' in os.environ else CONFIG['key']


def before_feature(context, feature):
    desired_capabilities = CONFIG['environments'][TASK_ID]

    for key in CONFIG["capabilities"]:
        if key not in desired_capabilities:
            desired_capabilities[key] = CONFIG["capabilities"][key]

    if 'BROWSERSTACK_APP_ID' in os.environ:
        desired_capabilities['app'] = os.environ['BROWSERSTACK_APP_ID']

    context.browser = webdriver.Remote(
        desired_capabilities=desired_capabilities,
        command_executor="http://%s:%s@hub-cloud.browserstack.com/wd/hub" % (BROWSERSTACK_USERNAME, BROWSERSTACK_ACCESS_KEY)
    )

    # desired_cap={
    #     "platformName": "Android",
    #     "platformVersion": "9.0",
    #     "deviceName": "CB5A201B3E",
    #     "udid": "192.168.56.101:5555",
    #     "app": "/Users/oleg/Downloads/DISCOVERY_1.9.5_apk-dl.com.apk",
    #     "appPackage":"discoveryloyalty.gha",
    #     # "appActivity":".activities.MainActivity",
    #     "appWaitActivity": "gha.discoveryloyalty.*",
    #     "automationName": "uiautomator2",
    #     "autoGrantPermissions": "true"
    # }
    # context.browser = webdriver.Remote("http://localhost:4723/wd/hub", desired_cap)

def after_feature(context, feature):
    context.browser.quit()
    # stop_local()