import time
from appium.webdriver.common.mobileby import MobileBy
from behave import then, given, when
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def wait_to_element(context, time_sec, css_element):
    i = 0
    status = False
    while i <= time_sec and status != True:
        try:
            context.browser.find_element_by_class_name(css_element)
            status = True
        except:
            pass
        time.sleep(1)
        i += 1


def wait_to_xpath(context, time_sec, xpath_element):
    i = 0
    status = False
    while i <= time_sec and status != True:
        try:
            context.browser.find_element_by_xpath(xpath_element)
            status = True
        except:
            pass
        time.sleep(1)
        i += 1


@given("I open app")
def step_impl(context):
    wait_to_element(context, 15, "android.widget.ImageButton")
    close_button = context.browser.find_element_by_class_name("android.widget.ImageButton")
    close_button.click()


@when('I enter a username "{usr}"')
def step_impl(context, usr):
    wait_to_xpath(context, 5, "//android.widget.EditText[contains(@resource-id,'username_edit_text')]")
    psw_input = context.browser.find_element_by_xpath("//android.widget.EditText[contains(@resource-id,'username_edit_text')]")
    psw_input.send_keys(usr)


@when('I enter a password "{psw}"')
def step_impl(context, psw):
    wait_to_xpath(context, 5, "//android.widget.EditText[contains(@resource-id,'password_edit_text')]")
    psw_input = context.browser.find_element_by_xpath("//android.widget.EditText[contains(@resource-id,'password_edit_text')]")
    psw_input.send_keys(psw)


@when("click on Sign in button")
def step_impl(context):
    signin_button = WebDriverWait(context.browser, 30).until(
        EC.element_to_be_clickable((MobileBy.ID, "discoveryloyalty.gha:id/sign_in_button"))
    )
    signin_button.click()


@then('"{user}" user signed in')
def step_impl(context, user):
    wait_to_xpath(context, 15, "//android.widget.TextView[contains(@resource-id,'member_name')]")
    full_name = context.browser.find_element_by_xpath("//android.widget.TextView[contains(@resource-id,'member_name')]")
    assert user == full_name.text